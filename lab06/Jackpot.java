public class Jackpot{ 
  public static void main(String[] args){ 
    
    System.out.println("Welcome to the game Jackpot :)"); 
    
    /*Inicializing and setting the values of the game*/
    Board gameBoard = new Board(); 
    boolean gameOver = false;
    int numOfTilesClosed = 0;
    
    /*Jackpot game loop*/ 
    while ( gameOver == false){ 
      System.out.println(gameBoard); 
      boolean currentGame = gameBoard.playATurn(); 
      if (currentGame == true){ 
        gameOver = true; 
      }
      else if (currentGame == false){ 
      }
      numOfTilesClosed = numOfTilesClosed + 1; 
    }
    /* win or lost*/
    if ( numOfTilesClosed >= 8){
      System.out.println("JACKPOT!! CONGRATULATIONS!! YOU WON!!"); 
    }else{ 
      System.out.println("YOU HAVE LOST!! CONTINUE?...");
    }
  }
}