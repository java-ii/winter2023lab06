import java.util.Random;
public class Die { 
  
  /*fields*/
  private int faceValue; 
  private Random diceRoll; 
  
  
  /*Constructor*/
  public Die(){ 
    
    this.faceValue = 1; 
    this.diceRoll = new Random(); 
  }
  
  
  
  /* Get Methods*/
  public int getFaceValue(){ 
    
    return this.faceValue;
  }
  
  /* Action Methods*/
  public void roll(){ 
    this.faceValue = diceRoll.nextInt(6) + 1;
  }
  
  public String toString(){ 
    return "The dice faceValue is " + this.faceValue;
  }
}