public class Board{ 
  
  private Die dice_1;
  private Die dice_2;
  private boolean[] tiles;
  
  /*Constructor*/
  public Board(){ 
    
    this.dice_1 = new Die();
    this.dice_2 = new Die(); 
    this.tiles = new boolean[12];
  }
  
  /*Action Methods*/
  public String toString(){ 
    
    /*for(int i=0; i< this.tiles.length; i++){
     if( tiles[i] == false){ 
     int num = i + 1;
     System.out.print(num + " ");
     }else if( tiles[i] == true){ 
     System.out.print("X ");     
     } 
     } 
     return " "; */
    String r = " ";
      for(int i=0; i<this.tiles.length; i++){ 
      if(tiles[i] == false){ 
        r += (i+1) + " "; 
      }else{ 
        r += "X "; 
      } 
    }
    return r; 
  } 
  
  public boolean playATurn(){ 
    boolean t = true; 
    boolean f = false;
    dice_1.roll(); 
    dice_2.roll();
    int roll_1 = dice_1.getFaceValue();
    int roll_2 = dice_2.getFaceValue();
    System.out.println(roll_1); 
    System.out.println(roll_2);  
    int sumOfDice = roll_1 + roll_2; 
    if(tiles[sumOfDice - 1] == f){ 
      tiles[sumOfDice - 1] = t; 
      System.out.println("Closing tile equal to sum: " + sumOfDice); 
      return f; 
    }else if (tiles[roll_1 - 1] == f){
      tiles[roll_1 - 1] = t; 
      System.out.println("Closing tile with the same value as die one: " + roll_1);
      return false;
    }else if (tiles[roll_2 - 1] == f){
      tiles[roll_2 - 1] = t; 
      System.out.println("Closing tile with the same value as die one: " + roll_2);
      return f;
    }else{
      System.out.println("All the tiles for these values are already shut"); 
      return t; 
    }
  }
}